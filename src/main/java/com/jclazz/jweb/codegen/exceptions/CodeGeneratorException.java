package com.jclazz.jweb.codegen.exceptions;

public class CodeGeneratorException extends RuntimeException {
    public static CodeGeneratorException withHint(String message, String hint,Throwable cause){
        return new CodeGeneratorException(message+":"+hint,cause);
    }
    public static CodeGeneratorException withHint(String message, String hint){
        return new CodeGeneratorException(message+":"+hint);
    }
    public CodeGeneratorException(String message) {
        super(message);
    }
    public CodeGeneratorException(String message, Throwable cause) {
        super(message, cause);
    }
    public CodeGeneratorException(Throwable cause) {
        super(cause);
    }

}
