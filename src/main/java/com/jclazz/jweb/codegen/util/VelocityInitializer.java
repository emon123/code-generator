package com.jclazz.jweb.codegen.util;

import com.jclazz.jweb.codegen.common.Constants;
import org.apache.velocity.app.Velocity;

import java.util.Properties;

public class VelocityInitializer {
    public static void initVelocity()
    {
        Properties p = new Properties();
        // 加载classpath目录下的vm文件
        p.setProperty("file.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        // 定义字符集
        p.setProperty(Velocity.ENCODING_DEFAULT, Constants.UTF8);
        p.setProperty(Velocity.OUTPUT_ENCODING, Constants.UTF8);
        // 初始化Velocity引擎，指定配置Properties
        Velocity.init(p);
    }
}
