package com.jclazz.jweb.codegen.common;

public class Constants {
    /**
     * UTF-8 字符集
     */
    public static final String UTF8 = "UTF-8";

    public static final String MYSQL_KEY_PRI="PRI";


    public static final char COMMA_CHAR=',';

    /**
     * 可显示在添加界面
     */
    public static final  String UI_SCOPE_ADD  = "A";
    /**
     * 可显示在编辑界面
     */
    public static final  String UI_SCOPE_EDIT = "E";
    /**
     * 可显示在列表中
     */
    public static final  String UI_SCOPE_LIST = "L";
    /**
     * 可显示在详情界面
     */
    public static final  String UI_SCOPE_DETAIL = "I";
}
