package com.jclazz.jweb.codegen.common;

import java.util.zip.ZipOutputStream;

public interface ZipGenerator {
    void generatorCode(ZipOutputStream zip);
}
