package com.jclazz.jweb.codegen.domain;

import lombok.Data;

import java.time.Instant;
import java.util.List;

@Data
public class TableMeta {
    private String tableName;
    private String tableComment;
    private Instant createTime;
    private Instant updateTime;
    private List<ColumnMeta> columns;
    private ColumnMeta primaryColumnRef;
}
