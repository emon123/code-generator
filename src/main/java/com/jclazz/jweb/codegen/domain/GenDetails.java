package com.jclazz.jweb.codegen.domain;

import lombok.Data;

@Data
public class GenDetails {
    /**
     * 作者
     */
    private String author;

    /**
     * 时间(YYYY-MM-DD)
     */
    private String date;

    /**
     * 基础包名，如 com.example.module
     */
    private String basePackageName;
    /**
     * 模块名,如sys.user
     */
    private String moduleName;
    /**
     * 服务ID,如sys-user
     * <br>用途：后端服务地址,权限标识
     */
    private String serviceId;

    /**
     * 业务对象名称
     */
    private String boName;

    /**
     * 业务对象类型
     */
    private ClassMeta domainType;
    /**
     * 原始表
     */
    private TableMeta tableMeta;

    /**
     * 新生成的菜单入口，也就是其父菜单的ID
     */
    private String menuEntry;
}
