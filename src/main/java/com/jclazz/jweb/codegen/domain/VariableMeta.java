package com.jclazz.jweb.codegen.domain;

import lombok.Data;

@Data
public class VariableMeta {
    private String dataType;
    private String name;
    /**
     * 类型风格的名称，如 user -> Name
     */
    private String nameNn;
    private String comment;

    private ColumnMeta columnBinding;
    /**
     *  ui显示标志
     * @see com.jclazz.jweb.codegen.common.Constants#UI_SCOPE_ADD
     * @see com.jclazz.jweb.codegen.common.Constants#UI_SCOPE_EDIT
     * @see com.jclazz.jweb.codegen.common.Constants#UI_SCOPE_LIST
     * @see com.jclazz.jweb.codegen.common.Constants#UI_SCOPE_DETAIL
     */
    private String uiScope;
}
