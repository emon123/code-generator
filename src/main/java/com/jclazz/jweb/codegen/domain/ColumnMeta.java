package com.jclazz.jweb.codegen.domain;

import lombok.Data;

/**
 *  information_schema.COLUMNS 表
 */
@Data
public class ColumnMeta {
    private String tableName;
    private String columnName;
    private String columnKey;
    private String extra;
    private String dataType;
    private int characterMaximumLength;
    private int ordinalPosition;
    private String columnComment;
}
