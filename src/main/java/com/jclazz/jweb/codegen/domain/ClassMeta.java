package com.jclazz.jweb.codegen.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ClassMeta {
    /**
     *  模块包名称
     */
    private String modulePackageName;
    /**
     *  类名称
     */
    private String className;
    /**
     *  类描述
     */
    private String comment;
    /**
     *  类成员
     */
    private List<VariableMeta> fields;
    /**
     *  主键列对应的类成员
     */
    private VariableMeta pkField;
}
