package com.jclazz.jweb.codegen.domain;

import lombok.Data;

@Data
public class GenerateSchema {
    /**
     * 作者
     */
    private String author;
    /**
     * 基础包名，如 com.example.module
     */
    private String basePackageName;
    /**
     * 子系统名，如 system.health
     */
    private String subSystem;
    /**
     * 表名称生成类名称时自动去除前缀(第一个'_'符号后字符用于生成类名，比如tb_user -> User)
     */
    private boolean autoRemovePrefix;

    /**
     * 不可以在列表中界面显示的字段(列表一般只显示简要信息)，逗号分隔
     */
    private String uiListExclude;
    /**
     * 不可以在添加时进行赋值的字段(如'create_time'之类的后端字段)，逗号分隔
     */
    private String uiAddExclude;
    /**
     * 不可以在编辑时进行赋值的字段(如'create_time'之类的后端字段)，逗号分隔<br>
     * 根据{@code uiDetailExclude}的配置，有的字段会以只读状态显示
     */
    private String uiEditExclude;
    /**
     * 不可以在详情界面显示的字段(如'create_time'之类的后端字段)，逗号分隔
     */
    private String uiDetailExclude;

    /**
     * 新生成的菜单入口，也就是其父菜单的ID,0 表示位于根菜单下
     */
    private String menuEntry;


}
