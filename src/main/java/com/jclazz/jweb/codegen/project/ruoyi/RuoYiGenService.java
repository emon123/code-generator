package com.jclazz.jweb.codegen.project.ruoyi;

import com.jclazz.jweb.codegen.common.ZipGenerator;
import com.jclazz.jweb.codegen.domain.ColumnMeta;
import com.jclazz.jweb.codegen.domain.GenerateSchema;
import com.jclazz.jweb.codegen.domain.TableMeta;
import com.jclazz.jweb.codegen.mapper.GenMapper;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.zip.ZipOutputStream;

@Service
public class RuoYiGenService {
    @Autowired
    private GenMapper genMapper;

    public byte[] generatorCode(String tableName){
        GenerateSchema config = new GenerateSchema();
        config.setAuthor("jclazz");
        config.setAutoRemovePrefix(true);
        config.setBasePackageName("com.ruoyi.project");
        config.setSubSystem("payment");
        config.setUiAddExclude("create_by,create_time,update_by,update_time,version");
        config.setUiDetailExclude("version");
        config.setUiEditExclude("order_id,create_by,create_time,update_by,update_time,version");
        config.setUiListExclude("create_by,create_time,update_by,update_time,version");
        config.setMenuEntry("0");

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ZipOutputStream zip = new ZipOutputStream(outputStream);
        // 查询表信息
        TableMeta table = genMapper.selectTableByName(tableName);
        // 查询列信息
        List<ColumnMeta> columns = genMapper.selectTableColumnsByName(tableName);
        table.setColumns(columns);
        // 生成代码
        ZipGenerator zipGenerator = new RuoYiCodeZipGenerator(config,table);
        zipGenerator.generatorCode(zip);
        IOUtils.closeQuietly(zip);
        return outputStream.toByteArray();
    }
}
