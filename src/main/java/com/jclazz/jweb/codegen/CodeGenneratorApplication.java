package com.jclazz.jweb.codegen;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan(basePackages = {"com.jclazz.jweb.codegen.mapper"})
public class CodeGenneratorApplication {

	public static void main(String[] args) {
		SpringApplication.run(CodeGenneratorApplication.class, args);
	}
}
